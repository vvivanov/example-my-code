# Shinservice components

## Install dependencies
```
npx lerna bootstrap
```


## How to add new package
```
cp example packages/NewPackageName  // copy example folder to packages dir
npx lerna bootstrap
```
That's it.


## How to install dependency to specific package (Example)
```
lerna add reactjs-popup --scope=@shinservice-components/profile-menu
```


## How to publish
```
git commit
git push
npm run release
```

## 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command               | Action                                                             |
|:----------------------|:-------------------------------------------------------------------|
| `npx lerna bootstrap` | Installs all package dependencies and links any cross-dependencies |
| `npm run build`       | Build all projects                                                 |
| `npm run clean`       | Remove node_modules directory from all packages.                   |
| `lerna publish`       | Publish packages that have changed since the last release          |
| `npm run release`     | Run CLI commands: `build`, `publish`                               |
| `npm run storybook`   | Run storybook with hot reload                                      |
