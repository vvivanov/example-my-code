import { FC } from 'react'

export interface NavigationItemInterface {
  id: number
  label: string
  active?: boolean
  url: string
  options?: {
    title?: string
  }
  items?: NavigationItemInterface[]
}

export type GeoPositionInterface = number[]

export interface RegionPhonePartsInterface {
  code: string
  city: string
  number: string
}

export interface RegionInterface {
  id: number
  slug: string
  title: string
  titleDeclination?: string
  email: string
  phone: string
  phoneParts: RegionPhonePartsInterface
  geoPosition: GeoPositionInterface
  url: string
}

export interface ItemMainRegion {
  geoPosition: number[]
  id: number
  parentId: number
  slug: string
  title: string
  url: string
}

export interface ToAllRegionInterface {
  geoPosition: null
  id: number
  items: ItemMainRegion[]
  parentId: number
  slug: null | string
  title: string
  url: string
}

export type LinkElement = {
  href: string
  description: string
  bagesCount: number | string
}

export type User = {
  name: string
  lastname: string
  is_loyalty_registered: boolean
  bonus: number | null
}

export type DataProfile = {
  linkList: LinkElement[]
  user: User
}
// ================================
export interface OptionsNav {
  class: string
  title: string
}

export interface ItemOptions {
  class: string
  title: string
}

export interface Item {
  id: number
  label: string
  url: string
  active: boolean
  encode: boolean
  options: ItemOptions
}

export interface Navigation {
  id: number
  label: string
  url: string
  active: boolean
  encode: boolean
  options: OptionsNav
  items: Item[]
}
//= ======================================
export interface Region {
  id: number
  priceZoneId: number
  slug: string
  title: string
  titleDeclination: string
  phone: string
  phoneParts: PhoneParts
  phones: Phone[]
  email: string[]
  delivery: Delivery
  geoPosition: number[]
  reviewCount: string
}

export interface Delivery {
  days: { [key: string]: string }
  notes: string
}

export interface PhoneParts {
  code: string
  city: string
  number: string
}

export interface Phone {
  key: string
  value: string
}
//= ==========================
export interface AllRegionsInterface {
  allRegions: AllRegions
}

export interface AllRegions {
  mainRegions: MainCity[]
  mainCities: MainCity[]
  regions: MainCity[]
}

export interface MainCity {
  id: number
  parentId: number
  slug: null | string
  title: string
  geoPosition: number[] | null
  url: null | string
  items?: MainCity[]
}
//= ========
export type Regions = AllRegionsInterface | undefined

export type LinkType = FC<{ href?: string; onClick?: () => void }>
