import styled from 'styled-components'

export const LinkStyled = styled.a`
  text-decoration: none;
`

export const LinkListItemStyled = styled.li`
  padding: 16px !important;
  border-bottom: 1px solid #d2d2d2;
  display: flex;
  align-items: center;
  background: #fff;
`

export const LinkNavigationStyled = styled(LinkStyled)`
  color: #000 !important;
  font-weight: 700 !important;
  font-size: 14px !important;
  cursor: pointer;
`

export const LinksListStyled = styled.ul`
  margin: 0 !important;
  padding: 0 !important;
  list-style-type: none !important;
  display: flex !important;
  flex-direction: column;
  margin-top: 16px !important;
  background: #fff;
`
