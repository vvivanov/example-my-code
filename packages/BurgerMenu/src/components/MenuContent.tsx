import React, { Fragment, useEffect, useMemo, useState } from 'react'
import styled from 'styled-components'
import MobileMenu from './MobileMenu'
import GeoIcon from '../icons/geoIcon.svg'
import { LinkListItemStyled, LinksListStyled } from './styled'
import {
  AllRegions,
  AllRegionsInterface,
  DataProfile,
  LinkType,
  Navigation,
  NavigationItemInterface,
  Region,
} from '../interfaces'

import DropDownList from './DropDownList'
import { Text } from './Text'
import AcornIcon from '../icons/icon-acorn.svg'
import ArrowRightIcon from '../icons/arrow-right.svg'
import ProfileIcon from '../icons/profile-in-button.svg'
import { Profile } from './Profile'
import { removeTokenOnClient } from '../helper'
import RegionsModal from './regions'

const MobileMenuInnerStyled = styled.div`
  display: flex;
  flex-direction: column;
  box-sizing: border-box !important;
  overflow-y: auto;
  height: 100%;

  .MobileMenuInnerStyled_AuthBtn {
    width: 100%;
  }

  .MobileMenuInnerStyled_AuthIcon {
    margin-left: 6px !important;
  }

  a.MobileMenuInnerStyled_Link {
    font-style: normal !important;
    font-weight: 400 !important;
    font-size: 14px !important;
    line-height: 140% !important;
    color: #828282 !important;
    text-decoration: none !important;
  }
`

const Button = styled.button`
  background: none;
  border: none;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  user-select: none;
  border-radius: 4px;
  background-color: #333333;
  padding: 10px 24px !important;
  font-size: 14px !important;
  font-weight: 700 !important;
  transition: background-color 0.25s ease-in-out;
  min-height: 40px;
  color: #ffffff;
`

const LinkNavigationStyled = styled.div<{ color?: string }>`
  font-weight: 700 !important;
  font-size: 14px !important;
  cursor: pointer;
  color: ${({ color }) => color ?? '#333'};
`

const ProfileWrapper = styled.div`
  padding: 16px !important;
  display: flex;
  flex-direction: column;
  gap: 15px;
  background: #fff;
`

interface NewNavigationItemInterface extends NavigationItemInterface {
  isOpen: boolean
}

interface LocalProps {
  show?: boolean
  setShow?: () => void
  navigation?: Navigation[] | undefined
  region: Region
  regions: AllRegions
  allRegionsStore: AllRegionsInterface
  dataProfile: DataProfile
  Link: LinkType
}

const MenuContent = (props: LocalProps) => {
  const { show, setShow, navigation, region, regions, allRegionsStore, dataProfile, Link } = props

  const [newNav, setNewNav] = useState<NewNavigationItemInterface[]>([])
  const [showRegions, setShowRegions] = React.useState<boolean>(false)
  const closeRegionsModal = () => {
    setShowRegions(false)
    document.body.style.overflow = 'hidden'
  }

  const isAuthorized: boolean = dataProfile.user.lastname !== '' || dataProfile.user.name !== ''

  useEffect(() => {
    if (navigation?.length) {
      const serializeNavigation = navigation.map((item: NavigationItemInterface) => {
        return {
          ...item,
          isOpen: false,
        }
      })
      setNewNav(serializeNavigation)
    }
  }, [navigation])

  const titleClickHandler = (title: string) => {
    const currentItem = newNav.find((item: NewNavigationItemInterface) => item.label === title)
    if (currentItem?.isOpen) {
      setNewNav(
        newNav.map((item: NewNavigationItemInterface) => ({
          ...item,
          isOpen: false,
        })),
      )
      return
    }
    setNewNav(
      newNav.map((item: NewNavigationItemInterface) => ({
        ...item,
        isOpen: currentItem?.label === item.label,
      })),
    )
  }

  const { customers, aboutCompany, options } = useMemo(() => {
    let customers
    let aboutCompany
    const options = newNav.filter((nav) => {
      if (nav.label === 'О компании') {
        aboutCompany = nav
        return false
      }

      if (nav.label === 'Покупателям') {
        customers = nav
        return false
      }
      return true
    })

    return { options, customers, aboutCompany }
  }, [newNav])

  const goToLogin = () => {
    if (typeof window !== 'undefined') {
      window.location.href = `/login`
    }
  }

  const LinkComponent = ({
    href,
    children,
    onClick,
    color,
    RightIcon,
    LeftIcon,
  }: {
    href?: string
    children: React.ReactNode | string
    onClick?: () => void
    color?: string
    RightIcon?: React.FC<any>
    LeftIcon?: React.FC<any>
  }) => (
    <Link href={href}>
      <LinkListItemStyled onClick={onClick}>
        {LeftIcon && <LeftIcon style={{ marginRight: 4 }} />}
        <LinkNavigationStyled color={color}>{children}</LinkNavigationStyled>
        {RightIcon && <RightIcon style={{ marginLeft: 'auto' }} />}
      </LinkListItemStyled>
    </Link>
  )

  return (
    <MobileMenu show={show} setShow={setShow} zIndex={1001} isTopFixed={showRegions}>
      <MobileMenuInnerStyled>
        <LinkComponent
          RightIcon={ArrowRightIcon}
          LeftIcon={GeoIcon}
          onClick={() => setShowRegions(true)}
        >
          {region.title}
        </LinkComponent>

        {!isAuthorized && (
          <ProfileWrapper>
            <Button className='MobileMenuInnerStyled_AuthBtn' onClick={goToLogin}>
              Войти
              <ProfileIcon style={{ marginLeft: 6 }} />
            </Button>
            <Text className='MobileMenuInnerStyled_AuthText'>
              Войдите в Личный кабинет для управления баллами, заказами, договорами хранения и
              автоматического заполнения данных.
            </Text>
          </ProfileWrapper>
        )}

        {isAuthorized && <Profile dataProfile={dataProfile} />}

        <LinksListStyled>
          <LinkComponent href={'/offers/'} color={'#CC0000'}>
            Акции и скидки
          </LinkComponent>
          {!dataProfile.user.is_loyalty_registered && (
            <LinkComponent LeftIcon={AcornIcon} href={'/disknt/'}>
              Программа лояльности
            </LinkComponent>
          )}
        </LinksListStyled>

        <LinksListStyled>
          {options.map((item: NewNavigationItemInterface) => (
            <Fragment key={item.id}>
              {item?.items?.length > 0 ? (
                <DropDownList
                  onShowMenu={setShow}
                  onClick={titleClickHandler}
                  showList={item.isOpen}
                  childrenList={item.items}
                  title={item.label}
                  Link={Link}
                />
              ) : (
                <LinkComponent onClick={setShow} href={item.url}>
                  {item.label}
                </LinkComponent>
              )}
            </Fragment>
          ))}
        </LinksListStyled>

        {(customers ?? aboutCompany) && (
          <LinksListStyled>
            {customers && (
              <DropDownList
                onShowMenu={setShow}
                onClick={titleClickHandler}
                showList={customers.isOpen}
                childrenList={customers.items}
                title={customers.label}
                Link={Link}
              />
            )}
            {aboutCompany && (
              <DropDownList
                onShowMenu={setShow}
                onClick={titleClickHandler}
                showList={aboutCompany.isOpen}
                childrenList={aboutCompany.items}
                title={aboutCompany.label}
                Link={Link}
              />
            )}
          </LinksListStyled>
        )}

        {isAuthorized && (
          <LinksListStyled>
            <LinkComponent href={'/logout'}>
              <a className='MobileMenuInnerStyled_Link' onClick={removeTokenOnClient}>
                Выйти из аккаунта
              </a>
            </LinkComponent>
          </LinksListStyled>
        )}
      </MobileMenuInnerStyled>

      {regions && showRegions && (
        <RegionsModal
          show={showRegions}
          setShow={closeRegionsModal}
          allRegions={allRegionsStore.allRegions}
        />
      )}
    </MobileMenu>
  )
}

export default MenuContent
