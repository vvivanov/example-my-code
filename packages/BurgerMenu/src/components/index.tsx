import React, {FC, useEffect, useState} from 'react'
import MenuContent from './MenuContent'
import { BurgerBtn } from './BurgerBtn'
import {
  AllRegions,
  AllRegionsInterface,
  DataProfile,
  LinkType,
  Navigation,
  Region,
} from '../interfaces'
import { LinkStyled } from './styled'

export type BurgerMenuProps = {
  region: Region
  regions: AllRegions
  navigation: Navigation[]
  allRegionsStore: AllRegionsInterface
  dataProfile: DataProfile
  Link?: LinkType | any
}

export const BurgerMenu: FC<BurgerMenuProps> = (props) => {
  const { region, regions, navigation, allRegionsStore, dataProfile, Link } = props

  const [showMenu, setShowMenu] = useState<boolean>(false)

  const toggleMenu = (evt: boolean): void => {
    setShowMenu(evt)
  }
  const appLnkClickHndler = (e: React.SyntheticEvent) => {
    const target = e.target as HTMLInputElement
    if (target.localName === 'a') {
      setShowMenu(false)
    }
  }

  useEffect(() => {
    if (typeof window !== 'undefined') {
      if (showMenu) {
        document.body.style.overflow = 'hidden'
        document.body.style.position = 'fixed'
      } else {
        document.body.style.overflow = 'auto'
        document.body.style.position = 'relative'
      }
    }
  }, [showMenu])

  return (
    <>
      <BurgerBtn onClickEvt={toggleMenu} open={showMenu} />
      <span onClick={appLnkClickHndler}>
        <MenuContent
          region={region}
          regions={regions}
          show={showMenu}
          navigation={navigation}
          allRegionsStore={allRegionsStore}
          dataProfile={dataProfile}
          Link={Link}
        />
      </span>
    </>
  )
}

BurgerMenu.defaultProps = {
  Link: LinkStyled,
}
