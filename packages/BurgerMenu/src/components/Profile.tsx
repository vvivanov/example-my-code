import React, { FC } from 'react'
import { LinksListStyled, LinkListItemStyled } from './styled'
import { DataProfile, LinkElement } from '../interfaces'
import styled from 'styled-components'
import ImgProfile from '../icons/profile.svg'
import IconAcorn from '../icons/icon-acorn.svg'
import { plural } from '../helper'

const ProfileWrap = styled.div`
  background: #fff;
  a {
    font-weight: 700 !important;
    font-size: 14px !important;
    cursor: pointer;
    text-decoration: none;
    color: inherit !important;
  }

  .Profile__UserImg {
    margin: 0 16px 0 0 !important;
  }

  .Profile__User {
    display: flex;
    align-items: center;
    padding: 16px !important;
    border-top: 1px solid #e5e5e5;
    border-bottom: 1px solid #e5e5e5;
  }

  .Profile__Title {
    font-style: normal !important;
    font-weight: 700 !important;
    font-size: 16px !important;
    line-height: 140% !important;
    letter-spacing: -0.03em !important;
    color: #333333;
  }
  .Profile__Text {
    font-style: normal !important;
    font-weight: 400 !important;
    font-size: 14px !important;
    line-height: 140% !important;
    color: #828282 !important;
  }
`

const ProfileLink = styled(LinkListItemStyled)`
  display: flex;
`
const ProfileLinkBonuses = styled(ProfileLink)`
  justify-content: space-between;

  span {
    display: flex;
    align-items: center;
  }
`

const Badge = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0px !important;
  width: 24px;
  height: 20px;
  background: #cc0000;
  border-radius: 10px;
  color: #fff;
`
const ProfileBadge = styled(Badge)`
  margin: 0 0 0 8px !important;
`

const ProfileList = styled(LinksListStyled)`
  margin: 0 !important;
`

type Props = {
  dataProfile: DataProfile
}

const concatNonNullValues = (...values: Array<string | undefined>): string => {
  const filledText = values.filter(Boolean)
  return filledText.join(' ')
}

const Profile: FC<Props> = (props) => {
  const { dataProfile } = props

  if (!dataProfile.linkList) return <></>

  return (
    <ProfileWrap>
      <a href='/my' className='Profile__User'>
        <span className='Profile__UserImg'>
          <ImgProfile />
        </span>
        <div>
          <div className='Profile__Title'>
            {concatNonNullValues(dataProfile.user.name, dataProfile.user.lastname)}{' '}
          </div>
          <div className='Profile__Text'>Личный кабинет</div>
        </div>
      </a>
      <ProfileList>
        {dataProfile.linkList.map((v: LinkElement) => (
          <ProfileLink key={v.href}>
            <a href={v.href}>{v.description}</a>
            {v.bagesCount > 0 && <ProfileBadge>{v.bagesCount}</ProfileBadge>}
          </ProfileLink>
        ))}
        {dataProfile.user.is_loyalty_registered && (
          <ProfileLinkBonuses>
            <a href='/my/bonuses'>Мои баллы</a>

            <span>
              <IconAcorn />
              {dataProfile.user.bonus ?? 0}{' '}
              {plural(Number(dataProfile.user.bonus) || 0, 'балл', 'балла', 'баллов')}
            </span>
          </ProfileLinkBonuses>
        )}
      </ProfileList>
    </ProfileWrap>
  )
}

export { Profile }
