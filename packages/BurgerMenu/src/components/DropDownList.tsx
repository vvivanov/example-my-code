import React from 'react'
import styled from 'styled-components'
import { LinkListItemStyled, LinkNavigationStyled } from './styled'
import ArrowBottom from '../icons/arrow-bottom.svg'
import { LinkType } from '../interfaces'

interface DropDawnListStyledInterface {
  show: boolean
}

interface ChildrenItemInterface {
  active: boolean
  encode: boolean
  id: number
  label: string
  options: {
    class: string
    title: string
  }
  url: string
}

interface LocalProps {
  childrenList?: ChildrenItemInterface[] | any
  title: string
  onClick?: (title: string) => void
  showList?: boolean
  onShowMenu?: () => void
  Link: LinkType
}

const DropDawnListStyled = styled.ul<DropDawnListStyledInterface>`
  margin: 0 !important;
  padding: 0 !important;
  display: ${({ show }) => (show ? 'block' : 'none')};
  width: 100%;
  list-style-type: none !important;
  flex-direction: column;
`

const LinkListItemBoldStyled = styled(LinkListItemStyled)`
  font-weight: bold !important;
  color: #333333 !important;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  cursor: pointer;
`

const LinkListItemWithoutBorderBottomStyled = styled(LinkListItemStyled)`
  background: #f7f7f7 !important;
  border: 1px solid #e5e5e5;
`

const LinkNavigationNormalWeightStyled = styled(LinkNavigationStyled)`
  font-weight: normal !important;
  border: none !important;
`

const ArrowImageWrapper = styled.div<{ rotate: any }>`
  width: 15px;
  height: 15px;
  transform: rotate(${({ rotate }) => (rotate ? '180deg' : '0deg')});
  transform-origin: 65% 65%;
  transition: 0.2s;
  cursor: pointer;
`

const DropDownList = ({ childrenList, title, onClick, showList, onShowMenu, Link }: LocalProps) => {
  const toggleSetShowList = () => onClick?.(title)

  return (
    <>
      <LinkListItemBoldStyled onClick={toggleSetShowList}>
        <span style={{ cursor: 'pointer', fontSize: 14 }}>{title}</span>
        <ArrowImageWrapper rotate={showList ? 1 : 0}>
          <ArrowBottom />
        </ArrowImageWrapper>
      </LinkListItemBoldStyled>
      {!!showList && (
        <DropDawnListStyled show={showList}>
          {childrenList.map((item: ChildrenItemInterface) => (
            <Link onClick={onShowMenu} key={item.url} href={item.url}>
              <LinkListItemWithoutBorderBottomStyled>
                <LinkNavigationNormalWeightStyled as={'div'}>
                  {item.label}
                </LinkNavigationNormalWeightStyled>
              </LinkListItemWithoutBorderBottomStyled>
            </Link>
          ))}
        </DropDawnListStyled>
      )}
    </>
  )
}

export default DropDownList
