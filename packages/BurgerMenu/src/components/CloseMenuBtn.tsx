import React from 'react'
import styled from 'styled-components'

const CloseMenuBtnWrapperStyled = styled.div`
  width: 50px;
  height: 50px;
  position: absolute;
  cursor: pointer;
  top: 20px;
  right: 0;
`

const CloseMenuBtnStyled = styled.a`
  position: absolute;
  right: 0;
  top: 0;
  width: 50px;
  height: 50px;
  opacity: 0.3;

  &:hover {
    opacity: 1;
  }

  &:before {
    position: absolute;
    left: 15px;
    content: ' ';
    height: 33px;
    width: 2px;
    background-color: #333;
    transform: rotate(45deg);
  }

  &:after {
    position: absolute;
    left: 15px;
    content: ' ';
    height: 33px;
    width: 2px;
    background-color: #333;
    transform: rotate(-45deg);
  }
`

interface LocalProps {
  setShow?: () => void
  withoutOverflow?: boolean
}

const CloseMenuBtn = (props: LocalProps) => {
  const { setShow, withoutOverflow } = props

  return (
    <CloseMenuBtnWrapperStyled
      onClick={
        withoutOverflow
          ? () => {}
          : () => {
              document.body.style.overflow = 'auto'
              document.body.style.position = 'relative'
            }
      }
    >
      <CloseMenuBtnStyled onClick={setShow} />
    </CloseMenuBtnWrapperStyled>
  )
}

export default CloseMenuBtn
