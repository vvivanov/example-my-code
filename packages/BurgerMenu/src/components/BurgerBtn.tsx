import React, { useState, FC, useEffect } from 'react'
import styled from 'styled-components'

const BG_COLOR = '#333333'
const WIDTH = '24px'
const HEIGHT = '2px'

const BurgerBtnHamburger = styled.div`
  font: inherit;
  display: inline-block;
  overflow: visible;
  margin: 0 !important;
  padding: 15px 0 !important;
  cursor: pointer;
  transition-timing-function: linear;
  transition-duration: 0.15s;
  transition-property: opacity, filter;
  text-transform: none;
  color: inherit;
  border: 0;
  background-color: transparent;

  &.BurgerBtn__Hamburger_Active {
    .BurgerBtn__HamburgerInner {
      transition-delay: 0.12s;
      transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
      transform: rotate(45deg);
    }
    .BurgerBtn__HamburgerInner:before {
      top: 0;
      transition: top 75ms ease, opacity 75ms ease 0.12s;
      opacity: 0;
    }
    .BurgerBtn__HamburgerInner:after {
      bottom: 0;
      transition: bottom 75ms ease, transform 75ms cubic-bezier(0.215, 0.61, 0.355, 1) 0.12s;
      transform: rotate(-90deg);
    }
  }

  .BurgerBtn__HamburgerBox {
    position: relative;
    display: inline-block;
    width: ${WIDTH};
  }

  .BurgerBtn__HamburgerInner {
    position: absolute;
    width: ${WIDTH};
    height: ${HEIGHT};
    transition-timing-function: ease;
    transition-duration: 0.15s;
    transition-property: transform;
    border-radius: ${HEIGHT};
    background-color: ${BG_COLOR};
    transition-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    transition-duration: 75ms;

    &:after {
      bottom: 6px;
      display: block;
      content: '';
      position: absolute;
      width: ${WIDTH};
      height: ${HEIGHT};
      transition-timing-function: ease;
      transition-duration: 0.15s;
      transition-property: transform;
      border-radius: ${HEIGHT};
      background-color: ${BG_COLOR};
      transition: bottom 75ms ease 0.12s, transform 75ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }

    &:before {
      top: 6px;
      display: block;
      content: '';
      position: absolute;
      width: ${WIDTH};
      height: ${HEIGHT};
      transition-timing-function: ease;
      transition-duration: 0.15s;
      transition-property: transform;
      border-radius: ${HEIGHT};
      background-color: ${BG_COLOR};
      transition: top 75ms ease 0.12s, opacity 75ms ease;
    }
  }
`

type Props = {
  onClickEvt: (active: boolean) => void
  open: boolean
}

export const BurgerBtn: FC<Props> = (props) => {
  const { onClickEvt, open } = props
  const [isActive, setActive] = useState<boolean>(open || false)
  const clickHdl = (active: boolean) => {
    onClickEvt(active)
    setActive(active)
  }
  useEffect(() => {
    setActive(open)
  }, [open])
  return (
    <BurgerBtnHamburger
      onClick={() => clickHdl(!isActive)}
      className={isActive ? 'BurgerBtn__Hamburger_Active' : ''}
    >
      <div className='BurgerBtn__HamburgerBox'>
        <div className='BurgerBtn__HamburgerInner'></div>
      </div>
    </BurgerBtnHamburger>
  )
}
