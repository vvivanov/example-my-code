import React, { PropsWithChildren, ReactNode, useEffect } from 'react'
import styled from 'styled-components'
// import { setVHByViewPort } from '../helper'
// import { throttle } from 'throttle-debounce'

interface MobileMenuStyledInterface {
  show?: boolean
  zIndex?: number
  isTopFixed?: boolean
}

export const HEADER_HEIGHT = '46px'

const MobileMenuStyled = styled.div<MobileMenuStyledInterface>`
  font-family: Arial, serif;
  position: fixed;
  width: 100%;
  transition: left 0.3s;
  z-index: 1001;
  box-sizing: border-box;
  top: ${HEADER_HEIGHT};
  bottom: 0;
  left: -100%;
  overflow: hidden;
  background: #eeeeee;

  ${({ show }) => (show ? `right: 0; left: 0;` : ``)}

  ${({ isTopFixed }) => isTopFixed && `position:fixed;top:0;transition: none;`}
`

interface LocalProps {
  children?: ReactNode
  show?: boolean
  setShow?: () => void
  zIndex?: number
  withoutOverflow?: boolean
  hideBtn?: boolean
  isTopFixed?: boolean
}

const MobileMenu = ({ children, ...props }: PropsWithChildren<LocalProps>) => {
  const { show, zIndex, isTopFixed } = props

  // const onScroll: any = throttle(1000, () => {
  //   setVHByViewPort()
  // })
  //
  // useEffect(() => {
  //   setVHByViewPort()
  // }, [])

  return (
    <MobileMenuStyled show={show} zIndex={zIndex} isTopFixed={isTopFixed}>
      {children}
    </MobileMenuStyled>
  )
}

export default MobileMenu
