import styled from 'styled-components'

export const Text = styled.p`
  font-family: inherit !important;
  font-size: 14px !important;
  font-weight: 400 !important;
  line-height: 19.6px !important;
  color: #333333 !important;
  margin: 0 !important;
`
