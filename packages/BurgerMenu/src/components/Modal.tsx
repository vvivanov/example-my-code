import React, { Component, ReactNode } from 'react'
import styled from 'styled-components'
import { HEADER_HEIGHT } from './MobileMenu'

const MOBILE = 576

export const ModalWrapper = styled.div`
  display: block;
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  overflow: hidden;
  z-index: 9999;
  height: 100%;
  background: #ffffff;
`

export const ModalContentWrapper = styled.div<{}>`
  display: flex;
  flex-direction: column;
  position: relative;
  box-sizing: border-box;
  margin: 0 auto !important;
  background: #ffffff;
  overflow: hidden;
  padding: 20px 0 16px 16px !important;
  height: 100%;
  width: 100%;
`

export const ModalContent = styled.div`
  display: block;
  position: relative;
  overflow-x: hidden;
  overflow-y: auto;

  ::-webkit-scrollbar {
    width: 6px;
    background: #cc0000;
  }
  ::-webkit-scrollbar-thumb {
    background: #cc0000;
  }
  ::-webkit-scrollbar-track {
    background: #e5e5e5;
  }
`

export const ModalBackdrop = styled.div`
  position: relative;
  box-sizing: border-box;
  padding: 50px 0 !important;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  display: flex;
  align-items: center;

  @media (max-width: ${MOBILE}px) {
    padding: 0 !important;
  }
`

const CloseMenuBtnStyled = styled.a`
  position: absolute;
  top: 30px;
  right: 12px;
  font-family: 'Arial', serif;
  font-style: normal;
  font-weight: 400 !important;
  font-size: 14px !important;
  line-height: 140%;
  color: #2f80ed !important;
  cursor: pointer;
  display: flex;
  align-items: center;
`
const CloseMenuIcon = styled.i`
  color: #cc0000;
  width: 20px;
  height: 20px;
  display: flex;
  justify-content: center;
`
export const ModalHeader = styled.div`
  margin: 0 !important;
  padding: 0 0 20px !important;
  font-family: 'Arial', serif !important;
  font-style: normal;
  font-weight: 700;
  font-size: 24px !important;
  line-height: 140%;
  letter-spacing: -0.03em;
  color: #333333;
`

export interface ModalProps {
  minHeight?: string
  width?: number
  modalWrapperClass: string
  showCloseButton: boolean
  escapeKeyClose: boolean
  onClose: () => void
}

export class Modal extends Component<ModalProps> {
  protected bodyOverflowValue: string

  static defaultProps = {
    header: null,
    modalWrapperClass: null,
    onClose: null,
    showCloseButton: true,
    escapeKeyClose: true,
  }

  componentDidMount(): void {
    this.bodyOverflowValue = document.body.style.overflow
    // document.body.style.overflow = 'hidden';
    window.addEventListener('keydown', this.handleEscapeKeyDown)
  }

  componentWillUnmount() {
    // document.body.style.overflow = 'auto';
    window.removeEventListener('keydown', this.handleEscapeKeyDown)
  }

  public handleEscapeKeyDown = (e: any): void => {
    const { onClose, escapeKeyClose } = this.props
    if (!escapeKeyClose) {
      return
    }
    if (e.keyCode === 27) {
      if (onClose) {
        onClose()
      }
    }
  }

  public handleBackDropClick = (e: any): void => {
    e.stopPropagation()
    const { onClose } = this.props
    if (onClose) {
      onClose()
    }
  }

  public handleCloseClick = (e: any): void => {
    e.preventDefault()
    e.stopPropagation()
    const { onClose } = this.props
    if (onClose) {
      onClose()
    }
  }

  public handleWrapperClick = (e: any): void => {
    e.stopPropagation()
  }

  public renderModalContent(): ReactNode {
    const { children } = this.props
    return (
      <ModalWrapper>
        <ModalBackdrop onClick={this.handleBackDropClick}>
          <ModalContentWrapper onClick={this.handleWrapperClick}>
            <ModalHeader>{'Выберите город'}</ModalHeader>
            <ModalContent className='ModalContent'>{children}</ModalContent>
            <CloseMenuBtnStyled onClick={this.handleCloseClick}>
              Закрыть<CloseMenuIcon>×</CloseMenuIcon>
            </CloseMenuBtnStyled>
          </ModalContentWrapper>
        </ModalBackdrop>
      </ModalWrapper>
    )
  }

  public render(): ReactNode {
    return this.renderModalContent()
  }
}
