import React from 'react'
import styled from 'styled-components'
import { MainCity } from '../../interfaces'
import { createUrl } from '../../helper'

const Item = styled.li<{ bold: boolean; extraMargin?: boolean }>`
  color: #333;
  font-size: 16px !important;

  ${({ bold }) =>
    bold &&
    `font-style: normal;
    font-size: 16px !important;
    font-weight: 700 !important;
    line-height: 140%;
    letter-spacing: -0.03em;
     color: #333333;`};

  ${({ extraMargin }) =>
    extraMargin &&
    `
    margin-bottom: 15px !important;
  `};
`

const Link = styled.a`
  color: #333;
  font-size: 14px !important;
  text-decoration: none;
  line-height: 19.6px !important;
`

interface LocalProps {
  mainRegions: MainCity[]
}

const isMoscow = (region: MainCity) => region.title.toLowerCase().includes('москва')
const isStPetersburg = (region: MainCity) => region.title.toLowerCase().includes('петербург')
const isBold = (region: MainCity) => isMoscow(region) || isStPetersburg(region)

const AllRegionsList = ({ mainRegions }: LocalProps) => {
  return (
    <>
      {mainRegions.map((region: MainCity) => (
        <Item key={region.id} extraMargin={isStPetersburg(region)} bold={isBold(region)}>
          <Link href={createUrl(region.slug)}>{region.title}</Link>
        </Item>
      ))}
    </>
  )
}

export default AllRegionsList
