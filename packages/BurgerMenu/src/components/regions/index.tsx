import React, { ChangeEvent, useMemo } from 'react'
import styled from 'styled-components'
import { AllRegions, ItemMainRegion, MainCity } from '../../interfaces'
import AllRegionsList from './AllRegionsList'
import { Modal } from '../Modal'
import SearchIcon from '../../icons/search-icon.svg'
import CrossIcon from '../../icons/cross.svg'
import { createUrl, replaceToStartArr } from '../../helper'

const MobileSearch = styled.div`
  padding: 0 10px 15px 0 !important;

  .MobileSearch__Input {
    background: #ffffff;
    border: 1px solid #a9a6ae;
    border-radius: 16px;
    padding: 0 16px !important;
    height: 32px;
    outline: none;
    font-style: normal;
    font-weight: 400;
    font-size: 16px !important;
    line-height: 140%;
    width: 100%;
    box-sizing: border-box;
    box-shadow: none !important;
  }

  .MobileSearch__InputAction {
    border: 0;
    background: transparent;
    position: absolute;
    top: 4px;
    right: 0;
    padding: 1px 6px !important;
  }
  
`

const TextFieldWrapperStyled = styled.div`
  position: relative;
  width: 100%;
`

const ListWrapper = styled.ul`
  display: flex;
  flex-direction: column;
  list-style-type: none;
  padding: 0 !important;
  margin: 0 !important;
  overflow: auto;
  gap: 5px;
`

const FoundedListItemLinkStyled = styled.a`
  color: #333;
  font-size: 14px !important;
  font-weight: 400 !important;
  line-height: 19.6px !important;
  text-decoration: none;
`

const getItemMainRegionList = (data: MainCity[]): ItemMainRegion[] => {
  const result: ItemMainRegion[] = []
  data.forEach((item) => result.push(...item.items))

  return result
}

const sortRegions = (a: ItemMainRegion, b: ItemMainRegion) => {
  const aa = a.title.toLowerCase()
  const bb = b.title.toLowerCase()

  if (aa < bb)
    // сортируем строки по возрастанию
    return -1
  if (aa > bb) return 1
  return 0 // Никакой сортировки
}

interface LocalProps {
  show?: boolean
  setShow?: () => void
  allRegions: AllRegions
}

const PRIMARY_CITIES = ['петербург', 'москва']

const RegionsModal = ({ show, setShow, allRegions }: LocalProps) => {
  const [searchValue, setSearchValue] = React.useState<string>('')

  const regions = React.useMemo(() => {
    const INITIAL_FIRST_SHOW = [
      ...allRegions.mainCities,
      ...getItemMainRegionList(allRegions.mainRegions),
    ]
    INITIAL_FIRST_SHOW.sort(sortRegions)
    return replaceToStartArr(INITIAL_FIRST_SHOW, PRIMARY_CITIES)
  }, [allRegions])

  const uniqueRegionsToSearch = React.useMemo(() => {
    const regionsWithDuplicates = [
      ...allRegions.mainCities,
      ...getItemMainRegionList(allRegions.mainRegions),
      ...getItemMainRegionList(allRegions.regions),
    ]

    return regionsWithDuplicates.filter(
      (region, i, list) =>
        list.findIndex((uniqueOrDuplicateRegion) => uniqueOrDuplicateRegion.id === region.id) === i,
    )
  }, [allRegions])

  const searchedRegions = useMemo(() => {
    if (!searchValue) {
      return []
    }

    const result = uniqueRegionsToSearch.filter((region) =>
      region.title.toLowerCase().includes(searchValue.toLowerCase()),
    )

    return result.length === 0 || result.length > 200 ? [] : result
  }, [searchValue])

  const resetSearch = () => setSearchValue('')

  if (!show) return null

  return (
    <Modal onClose={setShow}>
      <MobileSearch>
        <TextFieldWrapperStyled>
          <input
            className='MobileSearch__Input'
            type='text'
            value={searchValue}
            onChange={(e: ChangeEvent<HTMLInputElement>) => setSearchValue(e.currentTarget.value)}
            placeholder='Поиск региона'
          />
          {searchValue.length > 0 ? (
            <button className='MobileSearch__InputAction' onClick={resetSearch}>
              <CrossIcon />
            </button>
          ) : (
            <button className='MobileSearch__InputAction'>
              <SearchIcon />
            </button>
          )}
        </TextFieldWrapperStyled>
      </MobileSearch>

      {searchedRegions.length > 0 ? (
        <ListWrapper>
          {searchedRegions.map((region: MainCity) => (
            <li key={region.id}>
              <FoundedListItemLinkStyled href={createUrl(region.slug)}>
                {region.title}
              </FoundedListItemLinkStyled>
            </li>
          ))}
        </ListWrapper>
      ) : (
        <ListWrapper>
          <AllRegionsList mainRegions={regions} />
        </ListWrapper>
      )}
    </Modal>
  )
}

export default RegionsModal
