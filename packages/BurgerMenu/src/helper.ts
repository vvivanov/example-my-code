import Cookies from 'js-cookie'
const API_VI_TOKEN_NAME = 'API_VI_TOKEN'
const API_VI_TOKEN_INFO_NAME = 'API_VI_TOKEN_INFO'

export const createUrl = (subDomain: string | null) => {
  const { protocol, pathname, search, host } = window.location
  const currentHost = host.split('.')[0]
  const hostSubDomain = host.replace(currentHost, subDomain as string)

  return `${protocol}//${hostSubDomain}${pathname}${search}`
}

export function plural(n: number, one: string, two: string, five: string): string {
  if (isNaN(n)) return five
  n = Math.abs(n) % 100
  if ((n > 4 && n < 21) || (n %= 10) > 4 || n === 0) return five
  if (n > 1) return two
  return one
}

export const getGlobalDomain = (host: string | null): string => {
  let domain = host?.replace(/^([^\.]+\.)(.*)$/, '$2')

  if (domain?.includes(':')) {
    domain = domain.replace(/^([^:]+)(:[\d]*)$/, '$1')
  }
  return `.${domain ?? ''}`
}

export const removeTokenOnClient = (): void => {
  Cookies.remove(API_VI_TOKEN_NAME, {
    domain: getGlobalDomain(window.location.host),
    path: '/',
  })
  Cookies.remove(API_VI_TOKEN_INFO_NAME, {
    domain: getGlobalDomain(window.location.host),
    path: '/',
  })
}

export const replaceToStartArr = (arr, filterByTitle) => {
  const _arr = arr
  let filtredArr = []
  filterByTitle.forEach((v) => {
    if (filtredArr.length) {
      filtredArr = [filtredArr.find((el) => el.title.toLowerCase().includes(v))].concat(
        filtredArr.filter((el) => !el.title.toLowerCase().includes(v)),
      )
    } else {
      filtredArr = [_arr.find((el) => el.title.toLowerCase().includes(v))].concat(
        _arr.filter((el) => !el.title.toLowerCase().includes(v)),
      )
    }
  })
  return filtredArr
}

export const setVHByViewPort = () => {
  // получим значение вьюпорта с учетом браузерных  адресных строк и пр элементов браузера
  const vh = window.innerHeight * 0.01
  document.documentElement.style.setProperty('--vh', `${vh}px`)
}
