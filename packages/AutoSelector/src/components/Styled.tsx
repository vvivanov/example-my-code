import styled from 'styled-components'

export const StyledBackButton = styled.div`
  margin: 0 0 16px;

  a {
    text-decoration: none;
    vertical-align: middle;
    color: #2f80ed;

    &:hover {
      opacity: 0.8;
    }
  }
`

export const StyledList = styled.ul`
  display: flex;
  position: relative;
  flex-wrap: wrap;
  margin: 0;
  padding: 0;
  list-style: none;
  line-height: 1.4;

  > li {
    display: block;
    position: relative;
    box-sizing: border-box;
    margin: 0;
    //padding: 0 20px 20px 0;

    flex: 1 1 25%;
    //min-width: 200px;
    max-width: 350px;

    list-style: none;

    a {
      display: block;

      &:hover {
        color: #dd0000;
      }
    }
  }

  &:after {
    content: '';
    flex-grow: 1000;
  }
`

export const AlphaGroup = styled.dl`
  display: flex;
  position: relative;
  margin: 0;
  padding: 0;

  dt,
  dd {
    display: block;
    margin: 0;
    padding: 0;
    flex: 1 1 auto;
  }

  dt {
    flex: 0 0 30px;
    font-weight: bold;
    color: #dd0000;
  }
`

export const StyledListItems = styled.ul`
  display: block;
  position: relative;
  margin: 0;
  padding: 0;
  list-style: none;

  > li {
    display: block;
    position: relative;
    margin: 0;
    padding: 0;
    list-style: none;
  }
`

export const StyledRow = styled.tr<any>`
  display: flex;
  width: 100%;
  gap: 8px;

  & td,
  th {
    line-height: 140%;
    font-size: 14px;
    text-align: start;
    font-style: normal;
    font-weight: normal;
  }

  & td {
    display: flex;
    align-items: center;
    height: 44px;
  }

  & .s-hp {
    white-space: nowrap;
  }

  & th {
    font-size: 12px;
    color: #828282;
    height: 33px;

    text-overflow: ellipsis;
    overflow: hidden;
  }

  & .main-field {
    flex-grow: 1;
  }

  & .secondary-field {
    width: 15%;
  }

  @media (max-width: 991px) {
    && tr {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr 1fr;
    }

    & .main-field {
      flex-grow: unset;
    }

    & .secondary-field {
      width: auto;
    }
  }
`

export const StyledTable = styled.table`
  display: flex;
  flex-direction: column;
  width: 100%;

  & .content-row {
    cursor: pointer;
    border-bottom: 1px solid #e5e5e5;

    &:hover {
      color: #dd0000;
    }
  }

  & .content-row:last-child {
    border-bottom: none;
  }

  @media (max-width: 991px) {
    && tr {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr 1fr;
    }
  }
`

const PRELOADER =
  "data:image/svg+xml,%3C%3Fxml version='1.0' encoding='utf-8'%3F%3E%3Csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' style='margin: auto; background: transparent; display: block; shape-rendering: auto;' width='200px' height='200px' viewBox='0 0 100 100' preserveAspectRatio='xMidYMid'%3E%3Cg transform='rotate(0 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.9166666666666666s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(30 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.8333333333333334s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(60 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.75s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(90 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.6666666666666666s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(120 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.5833333333333334s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(150 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.5s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(180 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.4166666666666667s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(210 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.3333333333333333s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(240 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.25s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(270 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.16666666666666666s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(300 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='-0.08333333333333333s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3Cg transform='rotate(330 50 50)'%3E%3Crect x='47' y='24' rx='2.04' ry='2.04' width='6' height='12' fill='%231d0e0b'%3E%3Canimate attributeName='opacity' values='1;0' keyTimes='0;1' dur='1s' begin='0s' repeatCount='indefinite'%3E%3C/animate%3E%3C/rect%3E%3C/g%3E%3C/svg%3E"

export const ListWrapper = styled.div<{ isLoading: boolean }>`
  > ul:not(.generation-list-with-photo) {
    display: block;
    column-width: 180px;
    column-count: auto;

    @media (max-width: 480px) {
      column-width: 150px;
    }

    > li {
      margin: 0 20px 20px 0;
    }
  }

  dl {
    display: flex;
    page-break-inside: avoid;
    break-inside: avoid;
  }

  ul.generation-list-with-photo {
    display: grid;

    grid-template-columns: repeat(4, 1fr);
    justify-content: center;

    @media (max-width: 480px) {
      grid-template-columns: 1fr 1fr;
      grid-gap: 8px 16px;

      //grid-template-columns: repeat(auto-fill, minmax(20ch, 1fr));
    }
  }

  ${({ isLoading }) =>
    isLoading &&
    `
    opacity: 0.3;
    
    &::after {
      content: '';
      display: block;
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%,-50%);
      width: 64px;
      height: 64px;
      margin: auto;
      background: url("${PRELOADER}") center center/contain no-repeat;
    }
  `}

  .auto-select-popup-title {
    font-size: 24px;
    margin: 12px 0 24px 0;

    @media (max-width: 991px) {
      font-size: 14px;
      margin: 16px 0;
    }
  }

  & section .auto-select-popup-title {
    font-size: 16px;

    @media (max-width: 991px) {
      font-size: 14px;
      margin-top: 24px;
    }
  }

  ul li a {
    margin-bottom: 8px;
    &:hover {
      color: #dd0000 !important;
    }
  }

  @media (max-width: 480px) {
    > ul li {
      //max-width: 50%;
      min-width: 150px;
    }
  }

  @media (min-width: 991px) {
    && > ul li {
      flex: 1 1 15%;
    }
  }
`
