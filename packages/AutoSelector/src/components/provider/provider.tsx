import React, { FC, useCallback, useEffect, useState } from 'react'
import { AutoContext } from './context'
import { TAuto, TAutoFacets, TStep } from '../../interfaces'
import { createApi } from '../../api'
import { AutoSelectorModalProps } from '../AutoSelectorModal'

type Props = {
  facets: TAutoFacets
  setFacets: (facets: TAutoFacets) => void
} & AutoSelectorModalProps

export const AutoProvider: FC<Props> = ({
  children,
  auto,
  step = 'brand',
  onChangeStep,
  onAutoChange,
  onClose,
  isOpen,
  baseLink,
  setFacets,
  facets,
  apiUrl,
}) => {
  const [isLoading, setIsLoading] = useState(false)
  const [api] = useState(createApi(apiUrl))

  useEffect(() => {
    const isFacetsExist = Object.values(facets).some(Boolean)

    if (isFacetsExist) return

    setIsLoading(true)

    api
      .loadFacetsByAuto(auto)
      .then((facets) => setFacets(facets))
      .finally(() => setIsLoading(false))
  }, [auto])

  const onSetAuto = useCallback((auto: TAuto) => {
    onAutoChange(auto)
  }, [])

  const onSetStep = useCallback((step: TStep) => {
    onChangeStep(step)
  }, [])

  return (
    <AutoContext.Provider
      value={{
        isLoading,
        setIsLoading,

        facets,
        setFacets,

        auto,
        setAuto: onSetAuto,

        step,
        setStep: onSetStep,

        onClose,
        isOpen,

        baseLink,

        api,
      }}
    >
      {children}
    </AutoContext.Provider>
  )
}
