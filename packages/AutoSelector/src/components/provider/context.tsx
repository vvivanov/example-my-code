import { TAuto, TAutoFacets, TStep } from '../../interfaces'
import React from 'react'
import { createApi } from '../../api'

interface TContext {
  step: TStep
  setStep: (step: TStep) => void

  auto: TAuto
  setAuto: (auto: TAuto) => void

  facets: TAutoFacets
  setFacets: (facets: TAutoFacets) => void

  setIsLoading: (isLoading: boolean) => void
  isLoading: boolean

  baseLink?: string

  isOpen: boolean
  onClose: () => void

  api: ReturnType<typeof createApi>
}

export const AutoContext = React.createContext<TContext>({} as TContext)

export const useStore = () => React.useContext(AutoContext)
