import React, { FC, useCallback } from 'react'
import { StyledRow, StyledTable } from '../../Styled'
import { TModification } from '../../../interfaces'

interface Props {
  data: TModification[]

  onSelect: (entity: TModification) => void
}

export const ModificationListDetailed: FC<Props> = ({ data, onSelect }) => {
  const onClick = (item: TModification, e: React.MouseEvent<HTMLAnchorElement>): void => {
    e.preventDefault()
    onSelect(item)
  }

  const renderListItem = useCallback((item: TModification) => {
    const kw = item.kw ? `${+item.kw} кВт` : null
    const hp = item.hpFrom
      ? item.hpFrom === item.hpTo
        ? `${item.hpFrom} л.с.`
        : `${item.hpFrom} - ${item.hpTo} л.с.`
      : null

    return (
      <StyledRow
        key={item.id}
        className='content-row'
        onClick={(e: React.MouseEvent<HTMLAnchorElement>) => onClick(item, e)}
      >
        {!kw && (
          <td className='secondary-field'>
            {item.engineDisplacement
              ? parseFloat(item.engineDisplacement.toString())?.toFixed(1)
              : '—'}
          </td>
        )}
        <td className='secondary-field'>{item.engineTypeText ?? '—'}</td>
        <td className='secondary-field s-hp'>{kw ?? hp ?? '—'}</td>
        <td className='main-field'>{item.equipment ?? '—'}</td>
      </StyledRow>
    )
  }, [])

  const renderListItemTitle = useCallback((item: TModification) => {
    return (
      <StyledRow
        key={item.id}
        className='content-row'
        onClick={(e: React.MouseEvent<HTMLAnchorElement>) => onClick(item, e)}
      >
        <td className='main-field'>{item.title ?? '—'}</td>
      </StyledRow>
    )
  }, [])

  if (data.length === 0) return null

  const isNewFieldsEmpty = data.some(
    (item) => !item.kw && !item.hpFrom && !item.equipment && !item.engineDisplacement,
  )

  return (
    <StyledTable className='modification-list-detailed'>
      <thead>
        {!isNewFieldsEmpty && (
          <StyledRow key={'table-header'} className='table-header'>
            {!data[0].kw && <th className='secondary-field'>Объем двигателя</th>}
            <th className='secondary-field'>Тип двигателя</th>
            <th className='secondary-field'>Мощность двигателя</th>
            <th className='main-field'>Комплектация</th>
          </StyledRow>
        )}
      </thead>

      <tbody>{isNewFieldsEmpty ? data.map(renderListItemTitle) : data.map(renderListItem)}</tbody>
    </StyledTable>
  )
}
