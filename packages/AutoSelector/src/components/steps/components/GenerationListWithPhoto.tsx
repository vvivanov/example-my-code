import React, { FC } from 'react'
import styled from 'styled-components'
import { TGeneration } from '../../../interfaces'
import { StyledList } from '../../Styled'
import { TABLET_PORTRAIT } from '../../../constants'

const noPhotoImage =
  'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxyZWN0IHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIiBmaWxsPSJ3aGl0ZSIvPgo8cGF0aCBkPSJNMTE1LjkzOCAxNS41MDc4SDY2LjI1QzU5LjI5NDIgMTUuNTA3OCA1My41MDE5IDIwLjU4NDEgNTIuMzgyNyAyNy4yMjY2SDQ1LjE1NjJWMjIuNTM5MUgyNi40MDYyVjI3LjIyNjZIMjQuMDYyNUMxNi4zMDg0IDI3LjIyNjYgMTAgMzMuNTM1IDEwIDQxLjI4OTFWMTEwLjQzQzEwIDExOC4xODQgMTYuMzA4NCAxMjQuNDkyIDI0LjA2MjUgMTI0LjQ5MkgxMTUuOTM4QzEyMy42OTIgMTI0LjQ5MiAxMzAgMTE4LjE4NCAxMzAgMTEwLjQzVjI5LjU3MDNDMTMwIDIxLjgxNjIgMTIzLjY5MiAxNS41MDc4IDExNS45MzggMTUuNTA3OFpNMTIwLjYyNSAxMTAuNDNDMTIwLjYyNSAxMTMuMDE0IDExOC41MjIgMTE1LjExNyAxMTUuOTM4IDExNS4xMTdIMjQuMDYyNUMyMS40Nzc4IDExNS4xMTcgMTkuMzc1IDExMy4wMTQgMTkuMzc1IDExMC40M1Y0MS4yODkxQzE5LjM3NSAzOC43MDQ0IDIxLjQ3NzggMzYuNjAxNiAyNC4wNjI1IDM2LjYwMTZINTYuODc1QzU5LjQ2MzkgMzYuNjAxNiA2MS41NjI1IDM0LjUwMyA2MS41NjI1IDMxLjkxNDFWMjkuNTcwM0M2MS41NjI1IDI2Ljk4NTYgNjMuNjY1MyAyNC44ODI4IDY2LjI1IDI0Ljg4MjhIMTE1LjkzOEMxMTguNTIyIDI0Ljg4MjggMTIwLjYyNSAyNi45ODU2IDEyMC42MjUgMjkuNTcwM1YxMTAuNDNaTTMzLjQzNzUgNDUuOTc2Nkg0Mi44MTI1VjEwNS43NDJIMzMuNDM3NVY0NS45NzY2Wk05NS41NDY5IDMzLjU1NDdIMTExLjk1M1Y0Mi45Mjk3SDk1LjU0NjlWMzMuNTU0N1pNODEuOTUzMSA0Ny4xNDg0QzY1Ljc5ODggNDcuMTQ4NCA1Mi42NTYyIDYwLjI5MSA1Mi42NTYyIDc2LjQ0NTNDNTIuNjU2MiA5Mi41OTk2IDY1Ljc5ODggMTA1Ljc0MiA4MS45NTMxIDEwNS43NDJDOTguMTA3NCAxMDUuNzQyIDExMS4yNSA5Mi41OTk2IDExMS4yNSA3Ni40NDUzQzExMS4yNSA2MC4yOTEgOTguMTA3NCA0Ny4xNDg0IDgxLjk1MzEgNDcuMTQ4NFpNODEuOTUzMSA5Ni4zNjcyQzcwLjk2ODIgOTYuMzY3MiA2Mi4wMzEyIDg3LjQzMDIgNjIuMDMxMiA3Ni40NDUzQzYyLjAzMTIgNjUuNDYwNCA3MC45NjgyIDU2LjUyMzQgODEuOTUzMSA1Ni41MjM0QzkyLjkzOCA1Ni41MjM0IDEwMS44NzUgNjUuNDYwNCAxMDEuODc1IDc2LjQ0NTNDMTAxLjg3NSA4Ny40MzAyIDkyLjkzOCA5Ni4zNjcyIDgxLjk1MzEgOTYuMzY3MlpNODEuNDg0NCA3MS4yODkxQzgxLjQ4NDQgNzMuODc4IDc5LjM4NTggNzUuOTc2NiA3Ni43OTY5IDc1Ljk3NjZDNzQuMjA4IDc1Ljk3NjYgNzIuMTA5NCA3My44NzggNzIuMTA5NCA3MS4yODkxQzcyLjEwOTQgNjguNzAwMiA3NC4yMDggNjYuNjAxNiA3Ni43OTY5IDY2LjYwMTZDNzkuMzg1OCA2Ni42MDE2IDgxLjQ4NDQgNjguNzAwMiA4MS40ODQ0IDcxLjI4OTFaIiBmaWxsPSIjRTVFNUU1Ii8+Cjwvc3ZnPgo='
interface Props {
  data: TGeneration[]

  baseLink?: string

  onSelect: (entity: TGeneration) => void
}

const ImageWrapper = styled.div`
  padding: 16px;

  @media (max-width: ${TABLET_PORTRAIT}px) {
    margin-bottom: 8px;
    background-color: rgba(0, 0, 0, 0.03);
    border-radius: 4px;
  }
`

const CarImageWrapper = styled.div`
  .car-image-wrapper {
    text-decoration: none;
    color: #333;
    text-align: center;
  }

  img {
    width: 100%;
    max-width: 190px;
  }

  .error-image {
    max-height: 72px;
  }
`

export const GenerationListWithPhoto: FC<Props> = ({ data, onSelect, baseLink }) => {
  const onClick = (item: TGeneration, e: React.MouseEvent<HTMLAnchorElement>): void => {
    e.preventDefault()
    onSelect(item)
  }
  return (
    <StyledList className='generation-list-with-photo'>
      {data.map((item) => (
        <li key={item.id} style={{ flex: '1 1 25%' }}>
          <CarImageWrapper>
            <a
              className='car-image-wrapper'
              href={baseLink ? `${baseLink}${item.slug}/` : '#'}
              onClick={(e) => onClick(item, e)}
            >
              <ImageWrapper>
                <img
                  src={item?.image?.presets?.preview ?? ''}
                  alt={item.title}
                  onError={(e: React.SyntheticEvent<HTMLImageElement, Event>) => {
                    e.currentTarget.src = noPhotoImage
                    e.currentTarget.className = 'error-image'
                  }}
                />
              </ImageWrapper>
              {item.title}
            </a>
          </CarImageWrapper>
        </li>
      ))}
    </StyledList>
  )
}
