import React, { FC, useMemo } from 'react'
import { AlphaGroup, StyledList, StyledListItems } from '../../Styled'
import { TAutoCommon } from '../../../interfaces'

type ListByAlpha = Record<string, TAutoCommon[]>

interface Props {
  data: TAutoCommon[]
  baseLink?: string
  onSelect: (entity: TAutoCommon) => void
}

export const AlphaListComponent: FC<Props> = ({ data, ...props }) => {
  const list: ListByAlpha = useMemo(() => {
    if (data === null) return {}

    const result: ListByAlpha = {}
    data.forEach((entity: TAutoCommon) => {
      const alpha: string = entity.title[0].toUpperCase()
      if (!result[alpha]) {
        result[alpha] = []
      }
      result[alpha].push(entity)
    })
    return result
  }, [data])

  return (
    <StyledList>
      {Object.entries(list).map(([alpha, value]) => (
        <ListColumn key={alpha} {...props} alpha={alpha} list={value} />
      ))}
    </StyledList>
  )
}

type ListColumnProps = {
  alpha: string
  list: TAutoCommon[]
} & Pick<Props, 'baseLink' | 'onSelect'>

const ListColumn: FC<ListColumnProps> = ({ list, alpha, ...props }) => {
  return (
    <li key={alpha}>
      <AlphaGroup>
        <dt>{alpha}</dt>
        <dd>
          <StyledListItems>
            {list.map((item) => (
              <ListItem key={item.slug} item={item} {...props} />
            ))}
          </StyledListItems>
        </dd>
      </AlphaGroup>
    </li>
  )
}

type ListItemProps = {
  item: TAutoCommon
} & Pick<Props, 'baseLink' | 'onSelect'>

const ListItem: FC<ListItemProps> = ({ item, onSelect, baseLink }) => {
  const onClick = (e: React.MouseEvent<HTMLAnchorElement>): void => {
    e.preventDefault()
    onSelect(item)
  }

  return (
    <li key={item.id}>
      <a
        href={baseLink ? `${baseLink}${item.slug}/` : '#'}
        style={{ textDecoration: 'none', color: '#333' }}
        onClick={onClick}
      >
        {item.title}
      </a>
    </li>
  )
}
