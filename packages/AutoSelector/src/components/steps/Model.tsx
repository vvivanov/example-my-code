import React, { FC } from 'react'
import { TModel } from '../../interfaces'
import { AlphaListComponent } from './components/AlphaList'
import { useStore } from '../provider/context'

export const Model: FC = () => {
  const { facets, setFacets, setAuto, setStep, auto, setIsLoading, baseLink, api } = useStore()

  const onSelect = (model: TModel) => {
    setStep('generation')
    setAuto({ ...auto, model, generation: null, modification: null })
    setFacets({ ...facets, generation: null, modification: null })

    setIsLoading(true)
    api
      .loadFacetsByStep('generation', model.slug)
      .then((generation) => setFacets({ ...facets, generation, modification: null }))
      .finally(() => setIsLoading(false))
  }

  return <AlphaListComponent baseLink={baseLink} data={facets.model ?? []} onSelect={onSelect} />
}
