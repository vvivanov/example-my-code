import React, { FC } from 'react'
import { TBrand } from '../../interfaces'
import { AlphaListComponent } from './components/AlphaList'
import { useStore } from '../provider/context'

export const Brand: FC = () => {
  const { facets, setFacets, setAuto, setStep, setIsLoading, baseLink, api } = useStore()

  const onSelect = (brand: TBrand) => {
    setStep('model')
    setAuto({
      model: null,
      generation: null,
      modification: null,
      brand,
    })
    setFacets({
      ...facets,
      model: null,
      generation: null,
      modification: null,
    })

    setIsLoading(true)
    api
      .loadFacetsByStep('model', brand.slug)
      .then((model) =>
        setFacets({
          ...facets,
          model,
          generation: null,
          modification: null,
        }),
      )
      .finally(() => setIsLoading(false))
  }

  return <AlphaListComponent baseLink={baseLink} data={facets.brand ?? []} onSelect={onSelect} />
}
