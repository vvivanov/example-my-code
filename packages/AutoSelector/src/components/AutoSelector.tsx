import React, { FC } from 'react'
import { TStep } from '../interfaces'
import { Brand } from './steps/Brand'
import { Generation } from './steps/Generation'
import { Model } from './steps/Model'
import { Modification } from './steps/Modification'

import { ListWrapper } from './Styled'
import { BackButton } from './BackButton'
import { useStore } from './provider/context'

const COMPONENT_BY_STEP: Record<TStep, FC> = {
  brand: Brand,
  model: Model,
  generation: Generation,
  modification: Modification,
}

export const AutoSelectorComponent: FC = () => {
  const { step, isLoading } = useStore()

  const Component = COMPONENT_BY_STEP[step]

  const showBackButton = step !== 'brand'

  return (
    <ListWrapper isLoading={isLoading}>
      {showBackButton && <BackButton />}
      <Component />
    </ListWrapper>
  )
}
