import React, { FC, useState } from 'react'
import { AutoSelectorComponent } from './AutoSelector'
import { Modal } from './Modal'
import { TAuto, TAutoFacets, TStep } from '../interfaces'
import { AutoProvider } from './provider/provider'

const getModalTitle = (step: string, autoCurrent: TAuto) => {
  const brandLabel = autoCurrent.brand?.title ?? ''

  switch (step) {
    case 'brand':
      return 'Выберите марку'
    case 'model':
      return `Выберите модель ${brandLabel}`
    case 'generation':
      return `Выберите поколение ${brandLabel} ${autoCurrent.model?.title ?? ''}`
    case 'modification':
      return `Выберите модификацию ${brandLabel}
        ${autoCurrent.model?.title ?? ''} ${autoCurrent.generation?.title ?? ''}`
    default:
      return ''
  }
}

export interface AutoSelectorModalProps {
  step: TStep
  onChangeStep: (page: TStep) => void

  auto: TAuto
  onAutoChange: (auto: TAuto) => void

  baseLink?: string

  isOpen: boolean
  onClose: () => void

  apiUrl?: string
}

export const INITIAL_FACETS: TAutoFacets = {
  brand: null,
  model: null,
  generation: null,
  modification: null,
}

export const AutoSelectorModal: FC<AutoSelectorModalProps> = (props) => {
  const [facets, setFacets] = useState(INITIAL_FACETS)

  if (!props.isOpen) return null

  return (
    <Modal onClose={props.onClose} title={getModalTitle(props.step, props.auto)}>
      <AutoProvider facets={facets} setFacets={setFacets} {...props}>
        <AutoSelectorComponent />
      </AutoProvider>
    </Modal>
  )
}
