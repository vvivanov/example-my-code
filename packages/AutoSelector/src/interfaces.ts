export type TAutoCommon = {
  id: number
  code1c: string
  title: string
  slug: string
  parentId: string
}

export type TBrand = TAutoCommon

export type TModel = TAutoCommon

export type TGeneration = TAutoCommon & {
  isSubitemsOptional: boolean
  itemsCount: number
  releaseYearEnd: number
  releaseYearStart: number
  image: {
    generationId: number
    imageId: number
    presets: {
      default: string
      preview: string
    }
  }
}

export type TModification = TAutoCommon & {
  engineDisplacement: number
  engineType: string
  engineTypeText: string
  equipment: string
  hpFrom: number
  hpTo: number
  itemsCount: number
  kw: number
  releaseYearEnd: number
  releaseYearStart: number
}

export type TAuto = {
  brand: TBrand | null
  model: TModel | null
  generation: TGeneration | null
  modification: TModification | null
}

export type TAutoFacets = {
  brand: TBrand[] | null
  model: TModel[] | null
  generation: TGeneration[] | null
  modification: TModification[] | null
}

export type TStep = keyof TAuto
