import React, { useEffect, useState } from 'react'

import { ComponentMeta } from '@storybook/react'
import { AutoSelectorModal } from './src'
import { TAuto, TStep } from './src/interfaces'
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport'

const EMPTY_AUTO: TAuto = {
  brand: null,
  model: null,
  generation: null,
  modification: null,
}

const FILLED_CAR: TAuto = {
  model: {
    parentId: '1',
    id: 5,
    code1c: '403d246f-b14a-11ea-b78c-0050568041d3',
    title: 'MDX',
    slug: 'acura-mdx',
  },
  generation: {
    parentId: '2',
    isSubitemsOptional: false,
    id: 9,
    code1c: '403d2472-b14a-11ea-b78c-0050568041d3',
    title: 'II (YD2) SUV/2006-2013',
    slug: 'acura-mdx-ii-yd2-suv-2006-2013',
    releaseYearStart: 2006,
    releaseYearEnd: 2013,
    itemsCount: 2,
    image: {
      generationId: 9,
      imageId: 5349,
      presets: {
        default:
          '//www.shinservice.ru/media/auto/cached/acura/r800x800-kar/acura-mdx-ii-yd2-suv-2006-2013-3-5-awd_red_3202.png?iv=2',
        preview:
          '//www.shinservice.ru/media/auto/cached/acura/r276x174-kar/acura-mdx-ii-yd2-suv-2006-2013-3-5-awd_red_3202.png?iv=2',
      },
    },
  },
  modification: {
    id: 27,
    parentId: '123',
    code1c: '403d2473-b14a-11ea-b78c-0050568041d3',
    title: '3.5 AWD',
    slug: 'acura-mdx-ii-yd2-suv-2006-2013-3-5-awd',
    releaseYearStart: 2006,
    releaseYearEnd: 2013,
    engineType: 'p',
    engineTypeText: 'Бензин',
    engineDisplacement: 3.5,
    hpFrom: 256,
    hpTo: 256,
    kw: 123,
    equipment: 'AWD',
    itemsCount: 0,
  },
  brand: {
    id: 1,
    code1c: '403d246e-b14a-11ea-b78c-0050568041d3',
    title: 'ACURA',
    slug: 'acura',
    parentId: '12',
  },
}

export const EmptyCar = () => {
  const [auto, setAuto] = useState<TAuto>(EMPTY_AUTO)
  const [isOpen, setIsOpen] = useState(false)
  const [step, setStep] = useState<TStep>('brand')

  const onOpen = (step: TStep) => () => {
    setIsOpen(true)
    setStep(step)
  }

  return (
    <div>
      <button onClick={onOpen('brand')}>{auto.brand?.title ?? 'Брэнд'}</button>
      <button disabled={!auto.model} onClick={onOpen('model')}>
        {auto.model?.title ?? 'Модель'}
      </button>
      <button disabled={!auto.generation} onClick={onOpen('generation')}>
        {auto.generation?.title ?? 'Поколение'}
      </button>
      <button disabled={!auto.modification} onClick={onOpen('modification')}>
        {auto.modification?.title ?? 'Модификация'}
      </button>
      <AutoSelectorModal
        baseLink={'#'}
        apiUrl={'https://www.shinservice.ru'}
        step={step}
        onChangeStep={setStep}
        onAutoChange={setAuto}
        auto={auto}
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
      />
    </div>
  )
}

export const FilledCar = () => {
  const [auto, setAuto] = useState<TAuto>(EMPTY_AUTO)
  const [isOpen, setIsOpen] = useState(false)
  const [step, setStep] = useState<TStep>('brand')

  useEffect(() => {
    setTimeout(() => setAuto(FILLED_CAR), 1000)
  }, [])

  const onOpen = (step: TStep) => () => {
    setIsOpen(true)
    setStep(step)
  }

  return (
    <div>
      <button onClick={onOpen('brand')}>{auto.brand?.title ?? 'Брэнд'}</button>
      <button disabled={!auto.model} onClick={onOpen('model')}>
        {auto.model?.title ?? 'Модель'}
      </button>
      <button disabled={!auto.generation} onClick={onOpen('generation')}>
        {auto.generation?.title ?? 'Поколение'}
      </button>
      <button disabled={!auto.modification} onClick={onOpen('modification')}>
        {auto.modification?.title ?? 'Модификация'}
      </button>
      <AutoSelectorModal
        baseLink={'#'}
        step={step}
        apiUrl={'https://www.shinservice.ru'}
        onChangeStep={setStep}
        onAutoChange={setAuto}
        auto={auto}
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
      />
    </div>
  )
}

export default {
  title: 'AutoSelectorModal',
  component: AutoSelectorModal,
  parameters: {
    viewport: {
      viewports: INITIAL_VIEWPORTS,
    },
  },
} as ComponentMeta<typeof AutoSelectorModal>
