import { useState, useEffect } from 'react'
import { TABLET_PORTRAIT } from '../constants'

export function useIsMobile() {
  const [windowSize, setWindowSize] = useState<number>(0)
  useEffect(() => {
    function handleResize() {
      setWindowSize(window.innerWidth)
    }

    window.addEventListener('resize', handleResize)
    handleResize()

    return () => window.removeEventListener('resize', handleResize)
  }, [])

  return windowSize < TABLET_PORTRAIT
}
