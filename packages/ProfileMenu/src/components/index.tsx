import React, { FC } from 'react'
import { useIsMobile } from '../hooks/useIsMobile'
import { PopoverMenu } from './menu'
import { ProfileIcon } from './ProfileIcon'
import styled from 'styled-components'

const DEFAULT_LINKS: Links = {
  contracts: '/my/contract-storage',
  bonuses: { profile: '/my/bonuses', loyaltyInfo: '/disknt/' },
  orders: '/my/orders',
  logout: '/logout',
}

export const A = styled.a`
  text-decoration: none;
  color: #333333;
`

type Links = {
  orders: string
  bonuses: { profile: string; loyaltyInfo: string }
  contracts: string
  logout: string
}

type Icon = {
  onClick?: () => void
  href?: string | undefined
}

export type User = {
  firstName: string
  lastName: string
  bonuses: number | undefined
  contractsAmount: number | undefined
  ordersAmount: number | undefined
  is_loyalty_registered: boolean
}

export type RequiredProfileMenuProps = Required<ProfileMenuProps>

export type ProfileMenuProps = {
  icon?: Icon
  user: User | undefined
  Link?: React.ElementType<{ href?: string; onClick?: () => void }> | any
  links?: Links
}

export const ProfileMenu: FC<ProfileMenuProps> = ({
  user,
  links = DEFAULT_LINKS,
  Link = A,
  icon = {},
}) => {
  const isMobile = useIsMobile()
  const Component = isMobile || user === undefined ? ProfileIcon : PopoverMenu

  return <Component user={user} links={links} Link={Link} icon={icon} />
}
