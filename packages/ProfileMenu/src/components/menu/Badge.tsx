import React, { FC, ReactNode } from 'react'
import styled from 'styled-components'

type Props = {
  children: ReactNode | string | number
}

const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: #cc0000;
  color: #fff;
  width: fit-content;
  font-weight: bold;
  border-radius: 10px;
  min-width: 24px;
  min-height: 20px;
`

const validateNumber = (value: number): number => {
  return value >= 100 ? 99 : value
}

export const Badge: FC<Props> = ({ children }) => {
  const result = typeof children === 'number' ? validateNumber(children) : children
  return <Root>{result}</Root>
}
