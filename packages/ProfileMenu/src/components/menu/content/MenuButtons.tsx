import React, { FC } from 'react'
import { Text } from '../Text'
import styled from 'styled-components'
import { Badge } from '../Badge'
import AcornIcon from '../../../icons/acorn.svg'
import { RequiredProfileMenuProps } from '../../index'

function plural(number: number, words: [string, string, string]): string {
  const value = Math.abs(number) % 100
  const num = value % 10
  if (value > 10 && value < 20) return words[2]
  if (num > 1 && num < 5) return words[1]
  if (num === 1) return words[0]
  return words[2]
}

const priceWithSeparator = (price: number) => {
  const textPrice = price.toString()
  const length = textPrice.length

  switch (length) {
    case 4:
      return `${textPrice.slice(0, 1)} ${textPrice.slice(1, length)}`
    case 5:
      return `${textPrice.slice(0, 2)} ${textPrice.slice(2, length)}`
    case 6:
      return `${textPrice.slice(0, 3)} ${textPrice.slice(3, length)}`
    default:
      return `${textPrice}`
  }
}

const Container = styled.div`
  .buttons {
    padding: 16px;
    display: flex;
    gap: 8px;
    align-items: center;

    :hover {
      opacity: 0.7;
    }
  }

  .bonus {
    justify-content: space-between;
  }

  .bonusAmount {
    gap: 2px;
    display: flex;
    align-items: center;
  }

  .loyalty {
    padding: 13px 12px;
    gap: 2px;
    display: flex;
    align-items: center;

    :hover {
      opacity: 0.7;
    }
  }

  .buttons:not(:last-child) {
    border-bottom: 1px solid #e5e5e5;
  }

  a {
    outline: none;
  }
`

const getBonusLabel = (bonuses: number | undefined): string => {
  return `${priceWithSeparator(bonuses ?? 0)} ${plural(bonuses ?? 0, ['балл', 'балла', 'баллов'])}`
}

type Props = Omit<RequiredProfileMenuProps, 'fullName'>

export const MenuButtons: FC<Props> = ({ user, links, Link }) => {
  const { contractsAmount, ordersAmount, bonuses, is_loyalty_registered } = user ?? {}

  return (
    <Container>
      <Link href={links.orders}>
        <Text className={'buttons'} variant={'paragraph'} fontWeight={700}>
          {'Заказы'}
          <Badge>{ordersAmount ?? 0}</Badge>
        </Text>
      </Link>

      <Link href={links.contracts}>
        <Text className={'buttons'} variant={'paragraph'} fontWeight={700}>
          {'Договоры хранения'}
          <Badge>{contractsAmount ?? 0}</Badge>
        </Text>
      </Link>

      {is_loyalty_registered ? (
        <Link href={links.bonuses.profile}>
          <Text className={'buttons bonus'} variant={'paragraph'} fontWeight={700}>
            Мои баллы
            <div className={'bonusAmount'}>
              <AcornIcon alt={'bonuses'} />
              <Text>{getBonusLabel(bonuses)}</Text>
            </div>
          </Text>
        </Link>
      ) : (
        <Link href={links.bonuses.loyaltyInfo}>
          <div className={'loyalty'}>
            <AcornIcon alt={'bonuses'} />
            <Text variant={'paragraph'} fontWeight={700}>
              {'Программа лояльности'}
            </Text>
          </div>
        </Link>
      )}

      <Link href={links.logout}>
        <Text className={'buttons'} variant={'paragraph'} color={'#828282'}>
          Выйти из аккаунта
        </Text>
      </Link>
    </Container>
  )
}
