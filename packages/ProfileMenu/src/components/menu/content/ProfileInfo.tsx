import React, { FC } from 'react'
import AvatarIcon from '../../../icons/avatar.svg'
import { Text } from '../Text'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  padding: 16px;
  align-items: center;
  gap: 16px;
  border-bottom: 1px solid #e5e5e5; ;
`
type Props = {
  fullName: string
}

export const ProfileInfo: FC<Props> = ({ fullName }) => {
  return (
    <Container>
      <AvatarIcon />

      <div>
        <Text fontSize={'16px'} variant={'paragraph'} fontWeight={700}>
          {fullName ?? 'Профиль'}
        </Text>
        <Text variant={'paragraph'} color={'#828282'}>
          Личный кабинет
        </Text>
      </div>
    </Container>
  )
}
