import React, { CSSProperties, FC } from 'react'
import styled from 'styled-components'

type Props = {
  variant?: IVariantType
  as?: IRenderComponentType
  fontSize?: string
  fontWeight?: number
  margin?: string
  color?: string
  style?: CSSProperties | undefined
  display?: 'block' | 'inline'
  letterSpacing?: string
  className?: string
  onClick?: () => void
}

type IVariantType =
  | 'h1' /** font-size: 24px; font-weight: 700; */
  | 'h2' /** font-size: 20px; font-weight: 700; */
  | 'h3' /** font-size: 18px; font-weight: 700; */
  | 'h4' /** font-size: 16px; font-weight: 700; */
  | 'paragraph' /** font-size: 14px; font-weight: 400; */
type IAsType = keyof JSX.IntrinsicElements | React.ComponentType
type IRenderComponentType = 'h1' | 'h2' | 'h3' | 'h4' | 'p' | 'span' | IAsType

const defaultVariantMapping: Record<IVariantType, IRenderComponentType> = {
  h1: 'h1',
  h2: 'h2',
  h3: 'h3',
  h4: 'h4',
  paragraph: 'p',
}

const variantFontWeight = ({ variant }: Pick<Props, 'variant'>): number => {
  if (variant === 'paragraph') {
    return 400
  }
  return 700
}

const variantLineHeight = ({ variant }: Pick<Props, 'variant'>): string => {
  switch (variant) {
    case 'h1':
      return '33.6px'
    case 'h2':
      return '28px'
    case 'h3':
      return '25.2px'
    case 'h4':
      return '22.4px'
    case 'paragraph':
      return '19.6px'
    default:
      return '19.6px'
  }
}

const variantFontSize = ({ variant }: Pick<Props, 'variant'>): string => {
  switch (variant) {
    case 'h1':
      return '24px'
    case 'h2':
      return '20px'
    case 'h3':
      return '18px'
    case 'h4':
      return '16px'
    case 'paragraph':
      return '14px'
    default:
      return '14px'
  }
}

const normalizeMargin = (margin: string) => {
  return margin || `0 0 20px`
}

const StyledTypography = styled.span<Omit<Props, 'children'>>`
  font-family: 'Arial', serif;
  font-size: ${variantFontSize};
  font-weight: ${variantFontWeight};
  line-height: ${variantLineHeight};
  color: ${(props) => (props.color ? props.color : '#333333')};
  margin: 0;
  ${({ display }) => {
    return (
      !!display &&
      `
    display: ${display};
    `
    )
  }}

  ${({ margin }) =>
    !!margin &&
    `
    margin: ${normalizeMargin(margin)};
  `}
  
  
  ${({ letterSpacing }) =>
    !!letterSpacing &&
    `
  letter-spacing: ${letterSpacing};
  `}

  ${({ fontSize }) => {
    return (
      !!fontSize &&
      `
    font-size: ${fontSize};
    `
    )
  }}

  ${({ fontWeight }) => {
    return (
      !!fontWeight &&
      `
    font-weight: ${fontWeight};
    `
    )
  }}
`

export const Text: FC<Props> = ({ children, variant = 'paragraph', as, ...props }) => {
  const renderComponent = as || defaultVariantMapping[variant] || 'span'
  return (
    <StyledTypography {...props} as={renderComponent} variant={variant}>
      {children}
    </StyledTypography>
  )
}
