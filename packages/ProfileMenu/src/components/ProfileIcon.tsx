import React, { FC } from 'react'
import ProfileSvg from '../icons/profile-gray.svg'
import styled from 'styled-components'
import { TABLET_PORTRAIT } from '../constants'
import { useIsMobile } from '../hooks/useIsMobile'
import { RequiredProfileMenuProps } from './index'

const ContentWrapper = styled.div`
  position: relative;
  display: flex;
  cursor: pointer;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 82px;
  height: 56px;
  @media (max-width: ${TABLET_PORTRAIT}px) {
    height: unset;
    width: 100%;
  }
`

const ImageWrapper = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 100%;
`

const Notification = styled.div`
  background: #CC0000;
  border-radius: 10px;
  color: #fff;
  text-align: center;
  font-weight: 700;
  position: absolute;
  box-sizing: border-box;

  display: flex;
  align-items: center;
  justify-content: center;

  @media (min-width: ${TABLET_PORTRAIT}px) {
    min-width: 24px;
    height: 20px;
    position: absolute;
    top: 0;
    right: 0;
    font-size: 14px;
    padding: 0 8px;

  }

  @media (max-width: ${TABLET_PORTRAIT}px) {
    top: -10px;
    right: -10px;
    min-width: 19px;
    height: 20px;
    background: #CC0000;
    position: absolute;
    color: #fff;
    font-size: 12px;
    padding: 0 6px;
    border: 2px solid #FFFFFF;
  }
}
`

const Label = styled.div`
  color: #333333;
  cursor: pointer;
  text-align: center;
  font-weight: 700;
  font-size: 14px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  width: 82px;

  @media (max-width: ${TABLET_PORTRAIT}px) {
    font-size: 12px;
  }
`

type Props = RequiredProfileMenuProps

export const ProfileIcon: FC<Props> = ({ user, icon, Link }) => {
  const { contractsAmount, ordersAmount, firstName } = user ?? {}
  const isMobile = useIsMobile()
  const label = user ? firstName : 'Войти'
  const notificationAmount = (contractsAmount ?? 0) + (ordersAmount ?? 0)
  return (
    <Link onClick={icon?.onClick} href={icon?.href}>
      <ContentWrapper>
        <ImageWrapper title={label}>
          <ProfileSvg width={24} height={24} alt={label} />
        </ImageWrapper>

        {!isMobile && <Label>{label}</Label>}
        {!!notificationAmount && <Notification>{notificationAmount}</Notification>}
      </ContentWrapper>
    </Link>
  )
}
