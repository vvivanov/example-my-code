export type TyreUsedFacetFilterDto = {
  width?: number;
  profile?: string;
  diameter?: string;
}
