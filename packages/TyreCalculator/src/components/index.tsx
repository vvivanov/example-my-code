import React, {FC, useEffect, useState} from 'react'
import {createApi} from "../api";
import {TyreUsedFacetFilterDto} from "../TyreUsedFacetFilterDto";
import {SelectField, TextField} from "@shinservice-works/react-ui-components";
import styled from 'styled-components'

import {Button, ButtonColor, ButtonSize, Heading, HeadingLevel} from '@shinservice-works/react-ui-components';
import {PriceWrapper} from "../StyleLib";

export type TyreCalculatorProps = {}

const SelectWrapper = styled.div`
  display: flex;
`

//todo: solve this hack
const SelectStyled = styled.div`
  width: 100%;
  & div {
    width: 100% !important;
  }
`


const ErrorMessageWrapper = styled.div`
  position: relative;
  display: flex;
  cursor: pointer;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: red;
`

const Wrapper = styled.div`
  background: #EEEEEE;
  max-width: 386px;
  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 16px;

  font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
  line-height: 110%;
`

const Header = styled.div`
  display: flex;
  justify-content: center;
  margin: 0 0 20px;
  padding: 0;
  font-size: 14px;
  line-height: 48px;
  font-weight: 700;
  border-bottom: 2px solid #e62a3e;
`

const NotificationStyle01 = styled.div`
  font-size: 12px;
  color: #828282;
  line-height: 130%;
`

const TempStyle01 = styled.div`
  margin: 0 0 10px;
  display: flex;
  justify-content: space-between;
`

const TempStyle02 = styled.div`
  font-size: 14px;
  font-weight: 700;
  color: #000;
`

const TempStyle03 = styled.div`
  font-weight: 400;
  font-size: 13px;
  color: #2f80ed;
  cursor: pointer;
`

const TempStyle04 = styled.div`
  width: 100%;
  margin-top: 16px;
`

const TempStyle00 = styled.div`
`

export const TyreCalculator: FC<TyreCalculatorProps> = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const apiUrl = `https://web-api.1121313.ru:8014`;
  const [api] = useState(createApi(apiUrl));

  let [widthList, setWidthList] = useState([]);
  let [profileList, setProfileList] = useState([]);
  let [diameterList, setDiameterList] = useState([]);

  let [calculatedPrice, setCalculatedPrice] = useState(0);
  let [multipliedPrice, setMultipliedPrice] = useState(0);
  let [selectedWidth, setSelectedWidth] = useState(null);
  let [selectedProfile, setSelectedProfile] = useState(null);
  let [selectedDiameter, setSelectedDiameter] = useState(null);
  let [errorMessage, setErrorMessage] = useState('');
  let [isInited, setIsInited] = useState(false);
  let [isSizesUsed, setIsSizesUsed] = useState(false);
  let numberOfWheelsOptions = [1, 2, 3, 4]
  let [numberOfWheels, setNumberOfWheels] = useState(1);
  
  const defaultValue = '0';

  useEffect(() => {
    if (!isInited) {
      loadTyreUsedFacet();
    }
  })

  const loadTyreUsedFacet = () => {
    setIsInited(true);
    setIsLoading(true)
    api
      .getTyreUsedFacet()
      .then((data) => {
        let tempWidth = [];
        let tempProfile = [];
        let tempDiameter = [];
        data.data.items.forEach(item => {
          if (tempWidth.indexOf(item.width) < 0) {
            tempWidth.push(item.width);
          }
          if (tempProfile.indexOf(item.profile) < 0) {
            tempProfile.push(item.profile);
          }
          if (tempDiameter.indexOf(item.diameter) < 0) {
            tempDiameter.push(item.diameter);
          }
        })
        setWidthList(tempWidth);
        setProfileList(tempProfile);
        setDiameterList(tempDiameter);
      })
      .finally(() => {
        setIsLoading(false);
      })
  }
  
  const calculateTyreUsedFacet = (testDto: TyreUsedFacetFilterDto) => {
    setCalculatedPrice(null);

    if (!isCalculateParamsValid(testDto)) {
      setErrorMessage('');
      return;
    }
    setIsLoading(true)
    api
      .getTyreUsedFacetCalculatedPrice(testDto)
      .then((dto) => {
        if (dto.status_code === 200) {
          if (dto.data?.price) {
            setCalculatedPrice(dto.data.price);
            setMultipliedPrice(dto.data.price * numberOfWheels);
          }
        }

        if (dto.errors?.length) {
          setErrorMessage(dto.errors[0].message);
        } else {
          setErrorMessage('');
        }
      })
      .catch((e) => {
        debugger;
      })
      .finally(() => {
        setIsLoading(false);
      })
  }
  
  const setIsSizesUsedFlag = (dto: TyreUsedFacetFilterDto) => {
    let result = false;
    
    debugger
    
    if (!!dto.width || !!dto.profile || !!dto.diameter) {
      result = true;
    } 
    
    setIsSizesUsed(result);
  }

  const isCalculateParamsValid = (dto: TyreUsedFacetFilterDto): boolean => {
    let result = true;

    if (!dto?.width || !dto?.profile || !dto?.diameter) {
      result = false;
    }

    if (dto?.width === defaultValue || dto?.profile === defaultValue || dto?.diameter === defaultValue) {
      result = false;
    }

    return result;
  }

  const loadTyreUsedFacetFitered = (dto: TyreUsedFacetFilterDto) => {
    setIsLoading(true)
    api
      .getTyreUsedFacetFiltered(dto)
      .then((data) => {
        let tempWidth = [];
        let tempProfile = [];
        let tempDiameter = [];

        if (data.data?.width) {
          data.data.width?.forEach(item => {
            if (tempWidth.indexOf(item) < 0) {
              tempWidth.push(item);
            }
          })

          setWidthList(tempWidth);
        }
        if (data.data?.profile) {
          data.data.profile?.forEach(item => {
            if (tempProfile.indexOf(item) < 0) {
              tempProfile.push(item);
            }
          })

          setProfileList(tempProfile);
        }

        if (data.data?.diameter) {
          data.data.diameter?.forEach(item => {
            if (tempDiameter.indexOf(item) < 0) {
              tempDiameter.push(item);
            }
          })

          setDiameterList(tempDiameter);
        }
        debugger;
      })
      .finally(() => {
        setIsLoading(false);
      })
  }

  const testX = () => {
    debugger;
  }

  const handleWidthChange = (event) => {
    setSelectedWidth(event.target.value);
    const testDto: TyreUsedFacetFilterDto = {
      width: event.target.value,
      profile: selectedProfile,
      diameter: selectedDiameter,
    }
    setIsSizesUsedFlag(testDto);
    loadTyreUsedFacetFitered(testDto);
    calculateTyreUsedFacet(testDto);
  }
  const handleProfileChange = (event) => {
    setSelectedProfile(event.target.value);
    const testDto: TyreUsedFacetFilterDto = {
      width: selectedWidth,
      profile: event.target.value,
      diameter: selectedDiameter,
    }
    setIsSizesUsedFlag(testDto);
    loadTyreUsedFacetFitered(testDto);
    calculateTyreUsedFacet(testDto);
  }
  const handleDiameterChange = (event) => {
    setSelectedDiameter(event.target.value);
    const testDto: TyreUsedFacetFilterDto = {
      width: selectedWidth,
      profile: selectedProfile,
      diameter: event.target.value,
    }
    setIsSizesUsedFlag(testDto);
    loadTyreUsedFacetFitered(testDto);
    calculateTyreUsedFacet(testDto);
  }
  
  const resetSizes = () => {
    setSelectedWidth(defaultValue);
    setSelectedProfile(defaultValue);
    setSelectedDiameter(defaultValue);
    setCalculatedPrice(0);
    setMultipliedPrice(0);
    setIsSizesUsed(false);
  }
  
  const handleNumberOfWheelsChange = (event) => {
    setNumberOfWheels(event.target.value);
    setMultipliedPrice(calculatedPrice * event.target.value);
  }

  return (
    <Wrapper>
      <Header>
        <span>Калькулятор Trade in</span>
      </Header>
      <TempStyle01>
        <TempStyle02>
          Выберите параметры сдаваемых шин
        </TempStyle02>

        {isSizesUsed &&
            <TempStyle03 onClick={resetSizes}>
                Сбросить
            </TempStyle03>
        }
      </TempStyle01>

      <SelectWrapper>
        <SelectStyled style={{'margin-right': '11px'}}>
          <SelectField onChange={handleWidthChange}
                       value={selectedWidth}
                       label={'Ширина'}
                       style={{'margin-right': '11px'}}>
            <option value={defaultValue}>Все</option>

            {widthList?.map(item => {
              return <option value={item} key={item}>{item}</option>
            })}
          </SelectField>
        </SelectStyled>
        <SelectStyled style={{'margin-right': '11px'}}>
          <SelectField onChange={handleProfileChange}
                       value={selectedProfile}
                       label={'Профиль'}
                       style={{'margin-right': '11px'}}>
            <option value={defaultValue}>Все</option>

            {profileList?.map(item => {
              return <option value={item} key={item}>{item}</option>
            })}
          </SelectField>
        </SelectStyled>
        <SelectStyled>
          <SelectField onChange={handleDiameterChange}
                       value={selectedDiameter}
                       label={'Диаметр'}>
            <option value={defaultValue}>Все</option>

            {diameterList?.map(item => {
              return <option value={item} key={item}>{item}</option>
            })}
          </SelectField>
        </SelectStyled>
      </SelectWrapper>
      
      <TempStyle04>
        <SelectField onChange={handleNumberOfWheelsChange}
                     value={numberOfWheels}
                     label={'Количество'}
                     style={{width: '100%'}}>
          {numberOfWheelsOptions?.map(item => {
            return <option value={item} key={item}>{item}</option>
          })}
        </SelectField>
      </TempStyle04>
      
      <PriceWrapper>
        <Heading level={HeadingLevel.H2}>
          <span>Итого:</span>
          <span>{!calculatedPrice ? (<>&mdash;</>) : `${multipliedPrice}`} ₽ за {numberOfWheels} шт.</span>
        </Heading>
      </PriceWrapper>

      <NotificationStyle01>
        Предварительная стоимость шин в Trade in на сайте может отличаться от оценки,
        произведенной специалистом, в центре ШИНСЕРВИС.
      </NotificationStyle01>

      {errorMessage && <ErrorMessageWrapper>{errorMessage}</ErrorMessageWrapper>}
    </Wrapper>
  )
}

TyreCalculator.defaultProps = {
}
