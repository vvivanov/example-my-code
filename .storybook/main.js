module.exports = {
  stories: ["../packages/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: ["@storybook/addon-docs/preset", "@storybook/addon-essentials"],
  webpackFinal: async (config) => {
    const fileLoaderRule = config.module.rules.find(
        (rule) => rule.test && rule.test.test(".svg")
    );
    fileLoaderRule.exclude = /\.svg$/;

    config.module.rules.push({
      test: /\.svg$/,
      enforce: "pre",
      loader: require.resolve("@svgr/webpack")
    });

    return config;
  },
};
